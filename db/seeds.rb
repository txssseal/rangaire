# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
namespace :products do
  desc "Upload PDFs."
  task :create => :environment do
    @images = Dir["#{RAILS_ROOT}/public/spec_sheets/*.pdf"]
    @name 	= Dir["#{RAILS_ROOT}/public/name/*"]
    for image in @images
      Product.create(:spec => File.open(image),
      				 :spec_name => @name.each)
    end
  end
end