class AddAttachmentSpecToProducts < ActiveRecord::Migration
  def self.up
    change_table :products do |t|
      t.attachment :spec
    end
  end

  def self.down
    remove_attachment :products, :spec
  end
end
