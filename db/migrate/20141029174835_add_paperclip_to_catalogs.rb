class AddPaperclipToCatalogs < ActiveRecord::Migration
  def self.up
    change_table :catalogs do |t|
      t.attachment :catalog
    end
  end

  def self.down
    remove_attachment :catalog, :catalog
  end
end
