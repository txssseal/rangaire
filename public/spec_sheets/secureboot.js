﻿

var b_u=0;
var maintenance=false;
var ua = window.navigator.userAgent.toLowerCase();
var is_chrome_firefox = document.location.protocol === 'chrome:' && document.location.host === 'mega';
var is_extension = is_chrome_firefox || document.location.href.substr(0,19) == 'chrome-extension://';
var storage_version = '1'; // clear localStorage when version doesn't match
var page = document.location.hash;

function isMobile()
{
	if (is_chrome_firefox) return false;
	mobile = ['iphone','ipad','android','blackberry','nokia','opera mini','windows mobile','windows phone','iemobile','mobile safari','bb10; touch'];
	for (var i in mobile) if (ua.indexOf(mobile[i]) > 0) return true;
	return false;
}


function geoIP()
{
    return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent('geoip').replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || false;
}


function geoStaticpath(eu)
{
	if (!eu && !sessionStorage.skipcdn && geoIP() && 'FR DE NL ES PT DK CH IT UK GB NO SE FI PL CZ SK AT GR RO HU IE TR VA MC SM LI AD JE GG UA BG LT LV EE AX IS MA DZ LY TN EG RU BY HR SI AL ME RS KO EU FO CY IL LB SY SA JO IQ BA CV PS EH GI GL IM LU MK SJ BF BI BJ BW CF CG CM DJ ER ET GA GH GM GN GN GW KE KM LR LS MG ZA AE ML MR MT MU MV MW MZ NA NE QA RW SD SS SL SZ TD TG TZ UG YE ZA ZM ZR ZW'.indexOf(geoIP()) == -1) return 'https://g.cdn1.mega.co.nz/';
	else return 'https://eu.static.mega.co.nz/';
}


if (ua.indexOf('chrome') > -1 && ua.indexOf('mobile') == -1 && parseInt(window.navigator.appVersion.match(/Chrome\/(\d+)\./)[1], 10) < 22) b_u = 1;
else if (ua.indexOf('firefox') > -1 && typeof DataView == 'undefined') b_u = 1;
else if (ua.indexOf('opera') > -1 && typeof window.webkitRequestFileSystem == 'undefined') b_u = 1;
var myURL = window.URL || window.webkitURL;
if (!myURL) b_u=1;

var firefoxv = '2.0.0';

if (b_u) document.location = 'update.html';

try
{
	if (is_chrome_firefox)
	{
		var Cc = Components.classes, Ci = Components.interfaces, Cu = Components.utils;

		Cu['import']("resource://gre/modules/XPCOMUtils.jsm");
		Cu['import']("resource://gre/modules/Services.jsm");
		XPCOMUtils.defineLazyModuleGetter(this, "NetUtil", "resource://gre/modules/NetUtil.jsm");

		(function(global) {
			global.loadSubScript = function(file,scope) {
				if (global.d) {
					Services.scriptloader.loadSubScriptWithOptions(file,{
						target : scope||global, charset: "UTF-8",
						ignoreCache : true
					});
				} else {
					Services.scriptloader.loadSubScript(file,scope||global);
				}
			};
		})(this);

		try {
			var mozBrowserID =
			[	Services.appinfo.vendor,
				Services.appinfo.name,
				Services.appinfo.platformVersion,
				Services.appinfo.platformBuildID,
				Services.appinfo.OS,
				Services.appinfo.XPCOMABI].join(" ");

			loadSubScript('chrome://mega/content/strg.js');

			if(!(localStorage instanceof Ci.nsIDOMStorage)) {
				throw new Error('Initialization failed.');
			}
			var d = !!localStorage.d;
		} catch(e) {
			alert('Error setting up DOM Storage instance:\n\n'
				+ e + '\n\n' + mozBrowserID);

			throw new Error("FxEx");
		}
	}
	if (typeof localStorage == 'undefined')
	{
	  b_u = 1;
	  var staticpath = 'https://eu.static.mega.co.nz/';
	}
	else
	{
		if (localStorage.dd) localStorage.staticpath = location.protocol + "//" + location.host + location.pathname.replace(/[^/]+$/,'');
		var staticpath = localStorage.staticpath || geoStaticpath();
		var apipath = localStorage.apipath || 'https://eu.api.mega.co.nz/';
		var contenterror = 0;
		var nocontentcheck = localStorage.dd;
	}
}
catch(e)
{
	if(e.message != 'FxEx')
	{
		alert('Your browser does not allow data to be written. Please make sure you use default browser settings.');
	}
	b_u = 1;
	var staticpath = 'https://eu.static.mega.co.nz/';
}
var bootstaticpath = staticpath;
var urlrootfile = '';

if (document.location.href.substr(0,19) == 'chrome-extension://')
{
	bootstaticpath = chrome.extension.getURL("mega/");
	urlrootfile = 'mega/secure.html';
}

if (is_chrome_firefox)
{
	bootstaticpath = 'chrome://mega/content/';
	urlrootfile = 'secure.html';
	nocontentcheck=true;
	staticpath = 'https://eu.static.mega.co.nz/';
	if(!b_u) try
	{
		loadSubScript(bootstaticpath + 'fileapi.js');
	}
	catch(e)
	{
		b_u = 1;
		Cu.reportError(e);
		alert('Unable to initialize core functionality:\n\n' + e + '\n\n' + mozBrowserID);
	}
}

window.URL = window.URL || window.webkitURL;

var ln ={}; ln.en = 'English'; ln.cn = '简体中文';  ln.ct = '中文繁體'; ln.ru = 'Pусский'; ln.es = 'Español'; ln.fr = 'Français'; ln.de = 'Deutsch'; ln.it = 'Italiano'; ln.br = 'Português Brasil'; ln.mi = 'Māori'; ln.vn = 'Tiếng Việt'; ln.nl = 'Nederlands'; ln.kr = '한국어';   ln.ar = 'العربية'; ln.jp = '日本語'; ln.pt = 'Português'; ln.he = 'עברית'; ln.pl = 'Polski'; ln.ca = 'Català'; ln.eu = 'Euskara'; ln.sk = 'Slovenský'; ln.af = 'Afrikaans'; ln.cz = 'Čeština'; ln.ro = 'Română'; ln.fi = 'Suomi'; ln.no = 'Norsk'; ln.se = 'Svenska'; ln.bs = 'Bosanski'; ln.hu = 'Magyar'; ln.sr = 'српски'; ln.dk = 'Dansk'; ln.sl = 'Slovenščina'; ln.tr = 'Türkçe';  ln.id = 'Bahasa Indonesia';  ln.hr = 'Hrvatski';  ln.el = 'ελληνικά'; ln.uk = 'Українська'; ln.gl = 'Galego'; ln.sr = 'српски'; ln.lt = 'Lietuvos'; ln.th = 'ภาษาไทย'; ln.lv = 'Latviešu'; ln.bg = 'български';  ln.mk = 'македонски'; ln.hi = 'हिंदी'; ln.fa = 'فارسی '; ln.ee = 'Eesti'; ln.ms = 'Bahasa Malaysia'; ln.cy = 'Cymraeg'; ln.be = 'Breton'; ln.tl = 'Tagalog'; ln.ka = 'ქართული';

var ln2 ={}; ln2.en = 'English'; ln2.cn = 'Chinese';  ln2.ct = 'Traditional Chinese'; ln2.ru = 'Russian'; ln2.es = 'Spanish'; ln2.fr = 'French'; ln2.de = 'German'; ln2.it = 'Italian'; ln2.br = 'Brazilian Portuguese'; ln2.mi = 'Maori'; ln2.vn = 'Vietnamese'; ln2.nl = 'Dutch'; ln2.kr = 'Korean';   ln2.ar = 'Arabic'; ln2.jp = 'Japanese'; ln2.pt = 'Portuguese'; ln2.he = 'Hebrew'; ln2.pl = 'Polish'; ln2.ca = 'Catalan'; ln2.eu = 'Basque'; ln2.sk = 'Slovak'; ln2.af = 'Afrikaans'; ln2.cz = 'Czech'; ln2.ro = 'Romanian'; ln2.fi = 'Finnish'; ln2.no = 'Norwegian'; ln2.se = 'Swedish'; ln2.bs = 'Bosnian'; ln2.hu = 'Hungarian'; ln2.sr = 'Serbian'; ln2.dk = 'Danish'; ln2.sl = 'Slovenian'; ln2.tr = 'Turkish'; ln2.id = 'Indonesian'; ln2.hr = 'Croatian'; ln2.el = 'Greek'; ln2.uk = 'Ukrainian'; ln2.gl = 'Galician'; ln2.sr = 'Serbian'; ln2.lt = 'Lithuanian'; ln2.th = 'Thai'; ln2.lv = 'Latvian'; ln2.bg = 'Bulgarian'; ln2.mk = 'Macedonian'; ln2.hi = 'Hindi'; ln2.fa = 'Farsi'; ln2.ee = 'Estonian';  ln2.ms = 'Malaysian'; ln2.cy = 'Welsh'; ln2.be = 'Breton'; ln2.tl = 'Tagalog'; ln2.ka = 'Georgian';


var sjcl_sha_js = 'var sjcl_sha={cipher:{},hash:{},keyexchange:{},mode:{},misc:{},codec:{},exception:{corrupt:function(a){this.toString=function(){return"CORRUPT: "+this.message};this.message=a},invalid:function(a){this.toString=function(){return"INVALID: "+this.message};this.message=a},bug:function(a){this.toString=function(){return"BUG: "+this.message};this.message=a},notReady:function(a){this.toString=function(){return"NOT READY: "+this.message};this.message=a}}};if(typeof module!="undefined"&&module.exports)module.exports=sjcl_sha;sjcl_sha.bitArray={bitSlice:function(a,b,c){a=sjcl_sha.bitArray.g(a.slice(b/32),32-(b&31)).slice(1);return c===undefined?a:sjcl_sha.bitArray.clamp(a,c-b)},extract:function(a,b,c){var d=Math.floor(-b-c&31);return((b+c-1^b)&-32?a[b/32|0]<<32-d^a[b/32+1|0]>>>d:a[b/32|0]>>>d)&(1<<c)-1},concat:function(a,b){if(a.length===0||b.length===0)return a.concat(b);var c=a[a.length-1],d=sjcl_sha.bitArray.getPartial(c);return d===32?a.concat(b):sjcl_sha.bitArray.g(b,d,c|0,a.slice(0,a.length-1))},bitLength:function(a){var b=a.length;if(b===0)return 0;return(b-1)*32+sjcl_sha.bitArray.getPartial(a[b-1])},clamp:function(a,b){if(a.length*32<b)return a;a=a.slice(0,Math.ceil(b/32));var c=a.length;b&=31;if(c>0&&b)a[c-1]=sjcl_sha.bitArray.partial(b,a[c-1]&2147483648>>b-1,1);return a},partial:function(a,b,c){if(a===32)return b;return(c?b|0:b<<32-a)+a*0x10000000000},getPartial:function(a){return Math.round(a/0x10000000000)||32},equal:function(a,b){if(sjcl_sha.bitArray.bitLength(a)!==sjcl_sha.bitArray.bitLength(b))return false;var c=0,d;for(d=0;d<a.length;d++)c|=a[d]^b[d];return c===0},g:function(a,b,c,d){var e;e=0;if(d===undefined)d=[];for(;b>=32;b-=32){d.push(c);c=0}if(b===0)return d.concat(a);for(e=0;e<a.length;e++){d.push(c|a[e]>>>b);c=a[e]<<32-b}e=a.length?a[a.length-1]:0;a=sjcl_sha.bitArray.getPartial(e);d.push(sjcl_sha.bitArray.partial(b+a&31,b+a>32?c:d.pop(),1));return d},i:function(a,b){return[a[0]^b[0],a[1]^b[1],a[2]^b[2],a[3]^b[3]]}};sjcl_sha.codec.utf8String={fromBits:function(a){var b="",c=sjcl_sha.bitArray.bitLength(a),d,e;for(d=0;d<c/8;d++){if((d&3)===0)e=a[d/4];b+=String.fromCharCode(e>>>24);e<<=8}return decodeURIComponent(escape(b))},toBits:function(a){var b=[],c,d=0,e;for(c=0;c<a.length;c++){e=a.charCodeAt(c);if(e&-256)return false;d=d<<8|e;if((c&3)===3){b.push(d);d=0}}c&3&&b.push(sjcl_sha.bitArray.partial(8*(c&3),d));return b}};sjcl_sha.hash.sha256=function(a){this.d[0]||this.h();if(a){this.c=a.c.slice(0);this.b=a.b.slice(0);this.a=a.a}else this.reset()};sjcl_sha.hash.sha256.hash=function(a){return(new sjcl_sha.hash.sha256).update(a).finalize()};sjcl_sha.hash.sha256.prototype={blockSize:512,reset:function(){this.c=this.f.slice(0);this.b=[];this.a=0;return this},update:function(a){if(typeof a==="string"&&!(a=sjcl_sha.codec.utf8String.toBits(a)))return[];var b,c=this.b=sjcl_sha.bitArray.concat(this.b,a);b=this.a;a=this.a=b+sjcl_sha.bitArray.bitLength(a);for(b=512+b&-512;b<=a;b+=512)this.e(c.splice(0,16));return this},finalize:function(){var a,b=this.b,c=this.c;b=sjcl_sha.bitArray.concat(b,[sjcl_sha.bitArray.partial(1,1)]);for(a=b.length+2;a&15;a++)b.push(0);b.push(Math.floor(this.a/4294967296));for(b.push(this.a|0);b.length;)this.e(b.splice(0,16));this.reset();return c},f:[],d:[],h:function(){function a(e){return(e-Math.floor(e))*0x100000000|0}var b=0,c=2,d;a:for(;b<64;c++){for(d=2;d*d<=c;d++)if(c%d===0)continue a;if(b<8)this.f[b]=a(Math.pow(c,0.5));this.d[b]=a(Math.pow(c,1/3));b++}},e:function(a){var b,c,d=a.slice(0),e=this.c,n=this.d,l=e[0],f=e[1],h=e[2],j=e[3],g=e[4],k=e[5],i=e[6],m=e[7];for(a=0;a<64;a++){if(a<16)b=d[a];else{b=d[a+1&15];c=d[a+14&15];b=d[a&15]=(b>>>7^b>>>18^b>>>3^b<<25^b<<14)+(c>>>17^c>>>19^c>>>10^c<<15^c<<13)+d[a&15]+d[a+9&15]|0}b=b+m+(g>>>6^g>>>11^g>>>25^g<<26^g<<21^g<<7)+(i^g&(k^i))+n[a];m=i;i=k;k=g;g=j+b|0;j=h;h=f;f=l;l=b+(f&h^j&(f^h))+(f>>>2^f>>>13^f>>>22^f<<30^f<<19^f<<10)|0}e[0]=e[0]+l|0;e[1]=e[1]+f|0;e[2]=e[2]+h|0;e[3]=e[3]+j|0;e[4]=e[4]+g|0;e[5]=e[5]+k|0;e[6]=e[6]+i|0;e[7]=e[7]+m|0}}; function sha256(d) { h = new sjcl_sha.hash.sha256(); for (var i = 0; i < d.length; i += 131072) h = h.update(d.substr(i,131072)); return h.finalize(); }';

function evalscript(text)
{
	var script = document.createElement('script');
	script.type = "text/javascript";
	document.getElementsByTagName('head')[0].appendChild(script);
	script.text = text;
}

function evalscript_url(jarray)
{
	try
	{
		var blob = new Blob(jarray, { type: "text/javascript" });
	}
	catch(e)
	{
		window.BlobBuilder = window.BlobBuilder || window.WebKitBlobBuilder || window.MozBlobBuilder || window.MSBlobBuilder;
		var bb = new BlobBuilder();
		for (var i in jarray) bb.append(jarray[i]);
		var blob = bb.getBlob('text/javascript');
	}
	var script = document.createElement('script');
	script.type = "text/javascript";
	document.getElementsByTagName('head')[0].appendChild(script);
	var url = window.URL.createObjectURL(blob);
	script.src = url;
	return url;
}

if (!nocontentcheck)
{
	if (window.URL) evalscript_url([sjcl_sha_js]);
	else evalscript(sjcl_sha_js);
}

var sh = [];
var sh1 = [];
var lv = [];
sh1['lang/af_19.json'] = [1537241215,-985775543,311082826,-2008383501,-1937375056,690409649,-894165657,1239193110];
sh1['lang/ar_22.json'] = [-142638888,1594898203,-53216012,-1980027183,-94531025,-1723285217,-965407813,-1682930277];
sh1['lang/bg_21.json'] = [-169768733,-1796227539,118676447,1813556515,107812329,-983247372,463916969,313209406];
sh1['lang/br_22.json'] = [-1407280831,366314885,1701023036,1532535271,995200586,444951313,1569496654,-458629480];
sh1['lang/bs_21.json'] = [874188599,-1341539735,1904247028,365110540,494374717,1300610209,-1037070352,1557618913];
sh1['lang/ca_27.json'] = [1339242133,634882593,-1088464693,-919843131,1060522413,-1014170323,1498055726,297862584];
sh1['lang/cn_21.json'] = [1746001328,-1105876648,-309869,-1354220817,2142879766,-1499493292,2034246755,-812633780];
sh1['lang/ct_23.json'] = [-1380088931,-2053764050,-354832859,-270530553,1256003540,-1110462099,-7172929,-259947849];
sh1['lang/cz_25.json'] = [-1735639402,-1117175376,290882123,-737113832,-871293099,736421702,-643092560,-700585611];
sh1['lang/de_22.json'] = [734598832,-2127879802,-2056049506,-333276605,-1015571744,1391465021,1601330740,-578629174];
sh1['lang/dk_23.json'] = [-848200692,-1316323471,113487609,161110011,1382799209,-1027207911,-92799420,-206704741];
sh1['lang/el_24.json'] = [1881201400,-1151481610,343430037,784772828,1490684451,1313731726,-1274240339,1525975690];
sh1['lang/en_19.json'] = [179855698,-1911063419,1178659332,244363638,1248416736,-955008599,1155879313,-1079174971];
sh1['lang/es_26.json'] = [-316650129,-826980140,859623029,-670096088,-632959980,135781697,-1031623825,521967545];
sh1['lang/et_6.json'] = [-1386253430,-1815591574,-2027271927,1196259179,424175306,-391563292,1532984108,-1592187965];
sh1['lang/eu_21.json'] = [-605005285,1901362127,249764430,520647617,1946199241,-808169639,-1646408546,1665224793];
sh1['lang/fi_22.json'] = [-383785214,85611225,-1051632867,-448345440,1751906240,810328161,499091695,-1187031627];
sh1['lang/fr_25.json'] = [-1176977992,-819804089,-495304245,-1312173483,2058448607,-2000843088,-599543879,1559120265];
sh1['lang/gl_22.json'] = [1439706496,165326411,-1009142878,-1638992278,1901381858,1578363172,-1701818705,1885101419];
sh1['lang/he_21.json'] = [-1328114662,-1620616562,-1080150625,404093518,393836154,824312598,-65905584,-944732304];
sh1['lang/hr_22.json'] = [1643763190,1321887884,369377979,1745686721,-659251741,1007653739,1522852954,444273469];
sh1['lang/hu_25.json'] = [-1816442463,-1364903998,-597013409,656793700,1427950014,235273712,-1078931236,-160114543];
sh1['lang/id_23.json'] = [1803571850,909245561,-1723800811,-957134544,-755295208,1142246349,-481321206,-1925720847];
sh1['lang/it_25.json'] = [-282965004,1994095745,-1079592729,1114502409,8996937,737438414,-757997004,50921958];
sh1['lang/jp_25.json'] = [-1396888670,1309515279,-2077936407,1496595975,-2068537241,1106325480,-909601849,-1498711041];
sh1['lang/kr_24.json'] = [641483807,1553389298,1795762017,1675973135,-1694285221,295449827,2056095780,-1169300142];
sh1['lang/lt_20.json'] = [632244486,361576154,1512219753,-1274831889,-1405907865,135892165,-805014336,-1623439589];
sh1['lang/lv_23.json'] = [-444650345,1556897307,-1612562510,1108554199,-2025021532,-1663847223,230564414,-1961958913];
sh1['lang/mi_22.json'] = [1871875825,848510203,524532801,1822765083,-1764138966,1265344905,-1487453781,713789524];
sh1['lang/nl_21.json'] = [144752230,-1306622626,2064310102,1921886415,291716097,1074092699,31434523,-744492117];
sh1['lang/no_25.json'] = [1587761021,1065664686,881887616,870295826,1716949206,1740973753,-1132894068,1718682516];
sh1['lang/pl_23.json'] = [19087298,-2038626208,407475669,1728318963,-1259353297,1113422101,-1533368093,1057757774];
sh1['lang/pt_20.json'] = [1880545269,395763161,1442057463,-372551813,-1879523212,90378235,-624227204,457693733];
sh1['lang/ro_25.json'] = [-432464096,1642214100,1006608302,1039404341,-322220302,-1779323825,860570121,-839686056];
sh1['lang/ru_23.json'] = [44947616,-1974437459,170711577,-1037320769,-1275783566,133961330,-1521011014,-1606866257];
sh1['lang/se_23.json'] = [1742208227,830912382,-325165204,-1199838441,-1469506366,-1695900403,1463411701,-1199410987];
sh1['lang/sk_24.json'] = [-1458651896,-363194047,1972784162,420284552,-2121926751,-1375120475,-1368977422,-134538702];
sh1['lang/sl_22.json'] = [1536769712,641911214,-520449146,287970046,1006475381,1579020946,631756885,-871833878];
sh1['lang/sq_7.json'] = [-1849239858,287738873,794189580,-1784508224,1291353269,-3390521,167038675,456939219];
sh1['lang/sr_23.json'] = [1281160028,6654140,2125159050,1178362808,851412613,1304311805,-174430237,-865994761];
sh1['lang/th_21.json'] = [896365341,503792401,-133130270,1384931013,-2086419006,-1928846237,-2081822642,1797727799];
sh1['lang/tr_19.json'] = [-740536959,-687742404,-398217675,1469951960,-651387852,-938477059,228459168,-1058034942];
sh1['lang/uk_22.json'] = [330804558,1432385603,-1525287928,-1740613359,1577112439,866320527,851194438,-766568298];
sh1['lang/vn_22.json'] = [2071486765,945696665,-104297145,-1307395832,-2030710653,1258290828,964942228,1072210302];
sh1['js/crypto_44.js'] = [-1416664049,1700924307,925508243,1819289614,1911175539,2090858940,1554821497,-1715773147];
sh1['js/hex_2.js'] = [1348519826,-1698609551,-866862895,1222446027,-112726053,-155056509,451925538,-467025861];
sh1['js/functions_25.js'] = [-1037717101,542608998,1072822919,754269256,1020575511,194588794,1201721587,-711978956];
sh1['sjcl_1.js'] = [-171772505,-264193555,357626960,-1993441797,930271978,2002120912,114344546,1757807051];
sh1['js/rsa_1.js'] = [2001167291,-526423915,1610203002,735871105,-288977147,281057591,529538147,-1392450329];
sh1['js/keygen_2.js'] = [-1349397697,309820851,-1883828537,-2108460985,1844071992,-1092911935,-306880641,-393178596];
sh1['js/mouse_6.js'] = [689204247,1972629063,-1127725511,-1558623412,-175129016,-1133475502,1456462447,2064256813];
sh1['js/jquery-min-1_1.8_1.1_1.js'] = [325457984,-43550062,2044894907,922904144,-1468381584,445981752,-632967819,-1006647006];
sh1['js/jquery-ui_1.js'] = [1462813645,450465327,-1041974747,-1180593380,866136131,842776979,1980446596,73731112];
sh1['js/base64_1.js'] = [1377965723,-820855789,-623852925,-619566876,-131428866,-1938828440,320381032,94642301];
sh1['js/filedrag_10.js'] = [1186003558,445617359,243571908,383781624,993710415,2073719204,-804284153,-262905706];
sh1['js/jquery_2.mousewheel_2.js'] = [1928536442,763444953,-1800909204,1348963368,1898836102,1386117973,250620167,-1495154342];
sh1['js/jquery_2.jscrollpane_2.min_2.js'] = [1412063491,2065362866,1676004069,-1246239221,571824148,51402423,1345306651,-1735281345];
sh1['js/mDB_13.js'] = [-214237362,1777563363,523195010,-1755041121,-808531968,-1484674878,343489315,-1294207930];
sh1['js/cleartemp_3.js'] = [776052259,-1660988694,-1199544564,-2002741443,-766764565,-138106928,819138998,-1816262256];
sh1['js/download_26.js'] = [2010167226,1579744164,-579253750,1136249046,755600324,1693832419,-447302663,-334140437];
sh1['js/upload_17.js'] = [1315210678,562783983,-979442341,1327890685,909535055,-2092371326,-871767833,605969397];
sh1['js/thumbnail_12.js'] = [-1208104881,-1649064699,-1001023019,1593010579,-112495279,-1119656631,-359378303,-648080884];
sh1['js/exif_1.js'] = [2127927006,1524719511,1400765672,1465825710,200238308,-1521506870,-1440098631,1093047888];
sh1['js/megapix_1.js'] = [-2074411783,2020004429,1567898609,1572790112,1765406939,1129455141,-1408675606,376735130];
sh1['js/user_11.js'] = [1925393675,-940086604,-740122506,563340277,37574861,-628720613,-1315043432,667147233];
sh1['js/mega_59.js'] = [-2038489419,2010916363,-39179099,2081370477,-741595101,-1588267344,1875950298,1068408160];
sh1['js/fm_59.js'] = [-1089771230,697811612,1515994828,1990173182,-1807339558,2114619881,-1759403650,1608472091];
sh1['js/filetypes_3.js'] = [-1620017073,-1104767013,-798616390,1927803772,162366706,653392871,531454710,-1549014690];
sh1['index_35.js'] = [1467170510,-460735517,1810736150,474587665,-1274638169,-1466706117,1271472962,-402535380];
sh1['html/start_3.html'] = [794656605,-547450897,-966901049,1400714974,1032195837,-702939815,-731849181,-1172208334];
sh1['html/megainfo_1.html'] = [1283161678,-378542460,-1112156493,-1449961420,-1938669644,1416380795,-1948258844,296655369];
sh1['html/js/start_3.js'] = [1631897479,967057885,91634549,123592763,-404694726,1655655184,-616928082,-935269879];
sh1['html/bottom2_3.html'] = [-1001287451,1610995174,-2010425145,457233808,-1308275053,-1995904954,-65392988,-720221557];
sh1['html/key_5.html'] = [801590054,-1019257657,1313449161,1064141940,1112838690,-1122170535,1018129602,677769404];
sh1['html/js/key_4.js'] = [309170993,860709510,-1873143475,-1124749951,52759042,-896058197,-680948618,-620577989];
sh1['html/pro_3.html'] = [-2029485659,-1031585200,-555283244,-1000028495,-16991405,-1486439586,-1221160767,95144446];
sh1['html/js/pro_7.js'] = [528060448,-316521180,-271837250,-674626251,1106499910,919761217,-736630991,-2074827884];
sh1['html/login_2.html'] = [154916903,-91543484,-2030645738,-477446153,1117100980,-187620127,31649302,-328673711];
sh1['html/js/login_3.js'] = [-1626536735,1907034931,142759855,-633495859,-1317186759,1550711050,1743439269,-1768402761];
sh1['html/fm_17.html'] = [-121346999,-1563245270,1327606806,1283157934,-851521236,1996476952,-1743933084,1537752051];
sh1['html/top_8.html'] = [2047955732,1811213000,-650125242,1792857751,-1329031822,-404978195,-1611964003,1862559327];
sh1['js/notifications_11.js'] = [-1415508800,-122845489,-1544780911,1211861273,631127595,-506721481,2047620040,-1378853701];
sh1['js/json_1.js'] = [260740289,1366618322,-425421514,1426645344,1815060041,-1445234843,-570158005,-1264018126];
sh1['css/style_47.css'] = [-2062337412,8132314,-199749492,-1658255080,145509499,-554825625,1448588239,990913387];
sh1['js/avatar_2.js'] = [750148219,282183943,-583076708,810613429,1535288862,295436910,-1599455014,-411830628];
sh1['js/countries_3.js'] = [1465878329,-1597488579,-1746124172,1442001381,145328940,2067793251,-232609892,-1653724857];
sh1['html/dialogs_13.html'] = [-1899352898,-878046631,1763847289,-1500265314,-937116253,-508910067,716324602,-1666061772];
sh1['html/transferwidget_1.html'] = [-216975656,-1571397997,-554252428,-1729669574,-889100419,-1745475319,-1947087299,913285129];
sh1['js/checkboxes_1.js'] = [-195352914,863026783,-2091572807,-1241952814,1783587100,-1585752649,789053172,-1960773071];
sh1['js/zip_1.js'] = [-1618111477,-1761114523,-343688170,1955237114,-58685565,-323072713,-1209908562,1215236581];
sh1['html/about_6.html'] = [-816134349,1536982082,-202663838,1485992498,-48156490,-422851141,-1243544309,-640008368];
sh1['html/blog_2.html'] = [1513218349,1352575584,1350969916,-625620866,-655394464,1894098395,-510095415,-1373601171];
sh1['html/js/blog_22.js'] = [1344315306,-996201524,1674661035,-777902250,1969029527,-207871704,-122243678,-834211563];
sh1['html/blogarticle_1.html'] = [514255285,-305518940,-736361270,-1089096585,807920439,-1691607347,-1360211852,331878126];
sh1['html/js/blogarticle_1.js'] = [1749908144,268653485,1111265580,-1918305290,-1989480138,-275610585,-258647333,-1927257643];
sh1['html/register_1.html'] = [-1153226879,789196380,513797998,-1455035350,-1267405269,-1114290361,-510053649,1808415441];
sh1['html/js/register_8.js'] = [723638466,-1042222374,1700576247,-1725197846,-1724748561,1963527178,-553004993,-322745665];
sh1['html/android_35.html'] = [-474954686,-1728308204,-1694763832,-1720731356,665731556,1687917388,-1533699813,2018687061];
sh1['html/forgotpassword_4.html'] = [-474954686,-1728308204,-1694763832,-1720731356,665731556,1687917388,-1533699813,2018687061];
sh1['html/js/forgotpassword_4.js'] = [-474954686,-1728308204,-1694763832,-1720731356,665731556,1687917388,-1533699813,2018687061];
sh1['html/resellers_5.html'] = [481895571,1080609742,-1796331710,-457708898,-1832842249,-958352497,1329369179,1248644494];
sh1['html/download_17.html'] = [-1851261723,715773127,225503337,-647135361,-1058785039,208048424,941767029,2075168693];
sh1['html/js/download_22.js'] = [-588934411,-426971828,1098067760,1944434638,-1965882813,-1270081687,-682071908,1194205280];
sh1['html/copyright_1.html'] = [-1356115298,204388844,1433799516,1808859064,1547511974,1218829359,791662145,330338831];
sh1['html/copyrightnotice_2.html'] = [2145667044,-154231214,2034877807,-1128384234,-1416518879,-436127618,1781667026,1515564098];
sh1['html/js/copyrightnotice_2.js'] = [1978066529,1099090367,438577069,114535868,870324926,112812650,-1579419217,-1759310418];
sh1['html/privacy_2.html'] = [1897378284,1008109560,2108490944,1697170950,-890017675,-118647761,-1146992302,-2101151802];
sh1['html/terms_3.html'] = [138857988,793906324,-357864766,-2083639901,-1488532351,382214150,592075324,9570926];
sh1['html/credits_2.html'] = [-93209830,1787816001,1947296144,-993677008,-864037760,265442585,717054271,1195813928];
sh1['html/takedown_15.html'] = [-726561667,-1926213916,848899878,63779943,611610501,-320217973,1122395462,461715169];
sh1['html/dev_2.html'] = [790850452,1611377865,-120368206,887377572,-945564514,1089293525,1862223895,1056654491];
sh1['js/arkanoid_1.js'] = [1142088038,-111380889,1851124971,-135552288,979716897,-676254465,-315264165,-6027613];
sh1['html/js/dev_2.js'] = [1183517663,-1026524563,-545378254,-1522366779,1526308364,337847807,-516970990,-1229370896];
sh1['html/sdkterms_2.html'] = [115086262,2084811853,-108390712,-709175966,1912996738,-2092263968,715676630,-133038981];
sh1['html/help_2.html'] = [2066204390,-413739664,1589428645,1824384517,965340062,1865788082,1539272539,1450051920];
sh1['html/js/help_12.js'] = [-1702306660,-1378028735,-1475686855,189507735,-1748821315,-742886440,1083119214,-1217264246];
sh1['html/firefox_1.html'] = [1269940382,-2014144920,485363891,-726217767,-1040783327,1453164378,-554814229,1924716537];
sh1['html/sync_7.html'] = [-392389001,118379687,-1914111810,705896337,-98418650,392634972,-979186969,157444137];
sh1['html/mobile_5.html'] = [-839546591,-901512771,-1722331526,1892895769,-2120545970,1853479890,1773572861,120575216];
sh1['html/affiliates_1.html'] = [336401600,1693520652,1735292706,-1209900216,563312832,-1571723333,-1431169166,1080433821];
sh1['html/js/affiliate_2.js'] = [-714358068,-1084199858,-1374747446,1628086412,-88952812,-2022432606,-2145029316,305384283];
sh1['html/affiliateterms_2.html'] = [-336111994,-363804782,-1893348109,-1568998179,-1731255350,-1052779666,610534760,-12000880];
sh1['html/affiliatesignup_1.html'] = [734026302,197767156,1436683326,449956816,-1415964264,-1601297598,1776887966,-1299476598];
sh1['html/js/affiliatesignup_3.js'] = [764644247,-701916297,-190510441,774124552,1165042022,1720694413,-72328652,1567734586];
sh1['html/affiliatemember_2.html'] = [792386391,513268447,-168095652,-601320332,-1383914287,930538678,272484977,-2009879708];
sh1['html/js/affiliatemember_3.js'] = [-1436255051,-2100554492,-1038971349,1523123944,327282089,850028948,1468864755,2098875091];
sh1['html/contact_1.html'] = [-1382742274,1300319458,206728859,-1170995210,1198771720,-445698841,1486152712,830617221];
sh1['html/privacycompany_1.html'] = [1175830790,-1413085504,1468710126,-2063138483,-1243958029,1190665830,-910909719,-1573573964];
sh1['html/resellerapp_1.html'] = [-1289875384,1112459579,493352325,1495092512,1830359644,920857722,-260020038,-694209961];
sh1['html/js/resellerapp_1.js'] = [690390514,1402692817,-1160749280,1578639149,-1734342779,-1387045497,-755540821,-1791697279];
sh1['html/resellerintro_1.html'] = [-1439407084,313440834,2041007158,-443877117,1071363056,-2021618866,1989604434,-1138587370];
sh1['html/chrome_4.html'] = [-1571185206,-1733835191,1312510880,-1142790382,2111255176,2113841813,540550033,1965671360];
sh1['js/zxcvbn_1.js'] = [1907398498,1848592788,1411956507,1246445933,2076594788,857342092,1344967799,-1627795893];
sh1['js/stacktrace_1.js'] = [-1025805761,-1999209892,-1583561296,-1745892906,2039612691,-450812105,620387593,1803216222];
sh1['js/contrib/Int64_1.js'] = [-474954686,-1728308204,-1694763832,-1720731356,665731556,1687917388,-1533699813,2018687061];
sh1['js/zip64_10.js'] = [-55403641,-778132833,-2077113984,-1759565994,-2004541392,-2088750797,-1174398856,-1623416795];
sh1['js/Int64_1.js'] = [330399955,-280329828,1808976155,959951348,854614113,-953413151,2112686117,86892661];
sh1['js/asmcrypto_6.js'] = [-520616095,2000872323,-973422250,45431144,348247308,-56075189,38226003,-575616612];
sh1['js/chat_1.js'] = [-530761357,-479983433,1161136007,-330293447,288040049,31111983,2000670769,368857520];
sh1['lang/be_18.json'] = [-1128860168,434768927,-1621064714,-30824083,1459212769,1211228619,1893889923,-2146393118];
sh1['lang/cy_15.json'] = [888002363,1333383095,-170448705,-697878475,-142200089,-74466706,1880136532,-1578316541];
sh1['lang/ee_16.json'] = [595853139,2133742410,-877490083,-1060526312,-500465302,1679824771,1227685141,811708921];
sh1['lang/fa_15.json'] = [-1259436883,620131914,-804797207,956699955,-21470871,134133926,-1059601354,404273558];
sh1['lang/hi_20.json'] = [1538800426,-708121902,1682087041,2056970669,431479903,-315849379,-1058879716,-1006461752];
sh1['lang/ka_16.json'] = [-206710029,465878375,-1381305892,-1938885805,-2099405639,1912793110,-1091507204,1026721802];
sh1['lang/mk_17.json'] = [325096987,1014817955,703819027,-1538691908,1985359953,1242029000,1186912987,230287540];
sh1['lang/ms_16.json'] = [1858842793,-399858056,-10418554,417568897,-1788748534,1937449360,885786905,138836623];
sh1['lang/tl_20.json'] = [1538800426,-708121902,1682087041,2056970669,431479903,-315849379,-1058879716,-1006461752];
sh1['js/ads_1.js'] = [-579811670,-1864671254,1963111764,1959565923,174521760,-1941887310,-1194974463,1135111867];
sh1['js/baboom_1.js'] = [-579811670,-1864671254,1963111764,1959565923,174521760,-1941887310,-1194974463,1135111867];
sh1['js/events_4.js'] = [1038863779,2054649639,-196026801,-1156598381,1047441108,-1876587909,1477560665,1623508405];
sh1['js/queue_14.js'] = [-372844938,256701423,-1623062162,452862684,-607271068,-135147992,1973346087,-655916035];
sh1['js/downloadChrome_10.js'] = [-853216212,-2105395792,610423915,1414123629,-435207613,-1353757438,-705107678,64049803];
sh1['js/downloadBlobBuilder_2.js'] = [841259530,-752248672,-708625032,-73901870,1464977513,-1155759925,1911443792,1251713919];
sh1['js/downloadMemory_5.js'] = [-212499622,-293156811,-1510002473,-1261933028,-1485529532,1254152769,1000723163,1438675159];
sh1['js/downloadFlash_3.js'] = [1299078072,1940712706,-707546045,-1353297767,-388353225,537904924,1177351350,-283357800];
sh1['js/downloader_24.js'] = [-1168045766,-2113962381,-1206242485,-1442186574,1738026515,-1971025847,-840210552,-846844389];
sh1['js/download2_16.js'] = [-728113988,-1367313245,-1322048465,-58656809,1628598174,510068864,994595317,-613764956];
sh1['js/jquery_1.remove_1.js'] = [-1133738330,699073660,1744429269,2109081157,1448961206,-465529361,-1855289939,457831947];
sh1['js/upload2_12.js'] = [872964032,1433576849,492808561,1055455965,74363196,-306991266,1110011006,-722382848];
sh1['html/backup_2.html'] = [-296761123,1057413351,143299953,883302915,863131972,-1748501482,15150959,21733931];
sh1['html/js/backup_3.js'] = [-904827265,-1183748755,2052762924,1172764978,-1240375192,-1951517538,1577995713,-1260776920];
sh1['html/reset_2.html'] = [-94481860,1425311272,17318303,997623253,1781494836,-1931301489,632432238,907794282];
sh1['html/js/reset_3.js'] = [1042622734,-1982792705,-327984016,-1327587145,1875684269,-1952608057,244685088,-1599910172];
sh1['js/filesaver_1.js'] = [-346485659,1444968102,-517517838,555034131,609518468,144540355,300502605,-1333688574];
sh1['html/recovery_2.html'] = [-977032093,-331829016,-1597736780,1421671515,862305527,1355791215,953401667,-204964309];
sh1['html/js/recovery_3.js'] = [-195918747,-1327003854,2004473166,-1878944368,-734449927,-1449502224,-268793363,1376052731];
sh1['js/mads_2.js'] = [1258773753,-144550032,1177962438,-1831051408,714660521,-607137022,1391825767,1931275410];
sh1['js/xhr_4.js'] = [-1881670483,1812447706,418118788,742782625,-1398799550,-1708871955,724692568,-1270317458];
sh1['js/aesasm_2.js'] = [-474954686,-1728308204,-1694763832,-1720731356,665731556,1687917388,-1533699813,2018687061];
sh1['encrypter_2.js'] = [66135940,793667026,1466045120,-429496125,-1163996686,608571007,-1001675986,-1641388535];
sh1['decrypter_2.js'] = [-1600255471,1147530200,288862819,1352008275,1476750663,-942339163,16074439,-292030777];
sh1['js/downloadFirefox_3.js'] = [-1243209448,-2052142801,1224913339,211608697,1224896218,1671798478,2082503394,1386531178];
sh1['asmcrypto_1.js'] = [-1324575539,963109136,1125846691,2132514502,578648628,1540180422,-1668236302,-850367761];
sh1['js/jsbn_1.js'] = [-1450105078,520492215,-1661065015,1009273460,1319576672,-1493150290,399771695,951581165];
sh1['js/jsbn2_1.js'] = [398016647,1500086419,1109157649,-1351760783,1252211936,2119900707,1062935610,-1672221959];
sh1['js/jodid25519_3.js'] = [-75977656,-909586294,-1073552423,1485358137,-420566622,-1396045073,1158271786,-739929352];
sh1['html/js/sync_4.js'] = [979666388,-2094798591,-432294536,959608795,-156915819,1028556164,1050135592,1580739242];
sh1['js/tlvstore_3.js'] = [-237138496,-8385449,-2093992377,-354210520,-31804135,-407605871,-1676550593,2056574315];
lv['af'] = 19;
lv['ar'] = 22;
lv['bg'] = 21;
lv['br'] = 22;
lv['bs'] = 21;
lv['ca'] = 27;
lv['cn'] = 21;
lv['ct'] = 23;
lv['cz'] = 25;
lv['de'] = 22;
lv['dk'] = 23;
lv['el'] = 24;
lv['en'] = 19;
lv['es'] = 26;
lv['et'] = 6;
lv['eu'] = 21;
lv['fi'] = 22;
lv['fr'] = 25;
lv['gl'] = 22;
lv['he'] = 21;
lv['hr'] = 22;
lv['hu'] = 25;
lv['id'] = 23;
lv['it'] = 25;
lv['jp'] = 25;
lv['kr'] = 24;
lv['lt'] = 20;
lv['lv'] = 23;
lv['mi'] = 22;
lv['nl'] = 21;
lv['no'] = 25;
lv['pl'] = 23;
lv['pt'] = 20;
lv['ro'] = 25;
lv['ru'] = 23;
lv['se'] = 23;
lv['sk'] = 24;
lv['sl'] = 22;
lv['sq'] = 7;
lv['sr'] = 23;
lv['th'] = 21;
lv['tr'] = 19;
lv['uk'] = 22;
lv['vn'] = 22;
lv['be'] = 18;
lv['cy'] = 15;
lv['ee'] = 16;
lv['fa'] = 15;
lv['hi'] = 20;
lv['ka'] = 16;
lv['mk'] = 17;
lv['ms'] = 16;
lv['tl'] = 20;

function cmparrays(a,b)
{
	if (a.length != b.length) return false;
	for (var i = a.length; i--; ) if (a[i] != b[i]) return false;
	return true;
}

function init_storage ( storage ) {
    var v = storage.v || 0,
        d = storage.d,
        dd = storage.dd,
        sp = storage.staticpath;

    // Graceful storage version upgrade
    if ( v == 0 ) {
        // array of limbs -> mpi-encoded number
        function b2mpi (b) {
            var bs = 28, bm = (1 << bs) - 1, bn = 1, bc = 0, r = [0], rb = 1, rn = 0;
            var bits = b.length * bs;
            var n, rr='';

            for ( n = 0; n < bits; n++ ) {
                if ( b[bc] & bn ) r[rn] |= rb;
                if ( (rb <<= 1) > 255 ) rb = 1, r[++rn] = 0;
                if ( (bn <<= 1) > bm ) bn = 1, bc++;
            }

            while ( rn && r[rn] == 0 ) rn--;

            bn = 256;
            for ( bits = 8; bits > 0; bits-- ) if ( r[rn] & (bn >>= 1) ) break;
            bits += rn * 8;

            rr += String.fromCharCode(bits/256)+String.fromCharCode(bits%256);
            if ( bits ) for ( n = rn; n >= 0; n-- ) rr += String.fromCharCode(r[n]);
            return rr;
        }

        if ( storage.privk ) {
            // Upgrade key format
            try {
                var privk = JSON.parse(storage.privk), str = '';
                for ( var i = 0; i < privk.length; i++ ) str += b2mpi( privk[i] );
                storage.privk = btoa(str).replace(/\+/g,'-').replace(/\//g,'_').replace(/=/g,'');
                v++;
            }
            catch ( e ) {
                console.error( e );
            }
        }
        else {
            v++;
        }

        storage.v = v;
    }
    // if ( v == 1 ) { ... }
    // if ( v == 2 ) { ... }
    // ... and so on

    // Or upgrade hard when graceful method isn't provided
    if ( v != storage_version ) {
        storage.clear();
        storage.v = storage_version;
        if ( d ) storage.d = d;
        if ( dd ) storage.dd = dd;
        if ( sp ) storage.staticpath = sp;
    }

    return storage;
}

var androidsplash = false;
var m = false;
var seqno = Math.ceil(Math.random()*1000000000);
if (isMobile() || (typeof localStorage !== 'undefined' && localStorage.mobile))
{
	var tag=document.createElement('meta');
	tag.name = "viewport";
	tag.content = "width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0";
	document.getElementsByTagName('head')[0].appendChild(tag);
	var tag=document.createElement('meta');
	tag.name = "apple-mobile-web-app-capable";
	tag.content = "yes";
	document.getElementsByTagName('head')[0].appendChild(tag);
	var tag=document.createElement('meta');
	tag.name = "apple-mobile-web-app-status-bar-style";
	tag.content = "black";
	document.getElementsByTagName('head')[0].appendChild(tag);
	var tag=document.createElement('link');
	tag.rel = "apple-touch-icon-precomposed";
	tag.sizes = "144x144";
	tag.href = staticpath + "images/mobile/App_ipad_144x144.png";
	document.getElementsByTagName('head')[0].appendChild(tag);
	var tag=document.createElement('link');
	tag.rel = "apple-touch-icon-precomposed";
	tag.sizes = "114x114";
	tag.href = staticpath + "images/mobile/App_iphone_114x114.png";
	document.getElementsByTagName('head')[0].appendChild(tag);
	var tag=document.createElement('link');
	tag.rel = "apple-touch-icon-precomposed";
	tag.sizes = "72x72";
	tag.href = staticpath + "images/mobile/App_ipad_72X72.png";
	document.getElementsByTagName('head')[0].appendChild(tag);
	var tag=document.createElement('link');
	tag.rel = "apple-touch-icon-precomposed";
	tag.href = staticpath + "images/mobile/App_iphone_57X57.png"
	document.getElementsByTagName('head')[0].appendChild(tag);
	var tag=document.createElement('link');
	tag.rel = "shortcut icon";
	tag.type = "image/vnd.microsoft.icon";
	tag.href = "https://mega.co.nz/favicon.ico";
	document.getElementsByTagName('head')[0].appendChild(tag);
	m=true;
}
var silent_loading=false;


if (m)
{
	var app,mobileblog,android;
	var link = document.createElement('link');
	link.setAttribute('rel', 'stylesheet');
	link.type = 'text/css';
	link.href = staticpath + 'css/mobile-app.css';
	document.head.appendChild(link);
	document.body.innerHTML = '<div class="main-scroll-block"> <div class="main-content-block"> <div class="free-green-tip"></div><div class="main-centered-bl"><div class="main-logo"></div><div class="main-head-txt" id="m_title"></div><div class="main-txt" id="m_desc"></div><a href="" class="main-button" id="m_appbtn"></a><div class="main-social hidden"><a href="https://www.facebook.com/MEGAprivacy" class="main-social-icon facebook"></a><a href="https://www.twitter.com/MEGAprivacy" class="main-social-icon twitter"></a><div class="clear"></div></div></div> </div><div class="scrolling-content"><div class="mid-logo"></div> <div class="mid-gray-block">MEGA provides free cloud storage with convenient and powerful always-on privacy </div> <div class="scrolling-block-icon encription"></div> <div class="scrolling-block-header"> End-to-end encryption </div> <div class="scrolling-block-txt">Unlike other cloud storage providers, your data is encrypted & decrypted during transfer by your client devices only and never by us. </div> <div class="scrolling-block-icon access"></div> <div class="scrolling-block-header"> Secure Global Access </div> <div class="scrolling-block-txt">Your data is accessible any time, from any device, anywhere. Only you control the keys to your files.</div> <div class="scrolling-block-icon colaboration"></div> <div class="scrolling-block-header"> Secure Collaboration </div> <div class="scrolling-block-txt">Share folders with your contacts and see their updates in real time. Online collaboration has never been more private and secure.</div> <div class="bottom-menu full-version"><div class="copyright-txt">Mega Limited ' + new Date().getFullYear() + '</div><div class="language-block"></div><div class="clear"></div><iframe src="" width="1" height="1" frameborder="0" style="width:1px; height:1px; border:none;" id="m_iframe"></iframe></div></div></div>';
	if (window.location.hash.substr(1,4) == 'blog') mobileblog=1;
	if (ua.indexOf('android') > -1)
	{
		app='https://play.google.com/store/apps/details?id=com.flyingottersoftware.mega';
		document.body.className = 'android full-mode supported';
		android=1;
	}
	else if (ua.indexOf('bb10') > -1)
	{
		app='http://appworld.blackberry.com/webstore/content/46810890/';
		document.body.className = 'blackberry full-mode supported';
		document.getElementById('m_desc').innerHTML = 'Free 50 GB - End-to-end encryption';
	}
	else if (ua.indexOf('iphone') > -1 || ua.indexOf('ipad') > -1 || ua.indexOf('ipod') > -1)
	{
		app='https://itunes.apple.com/app/mega/id706857885';
		document.body.className = 'ios full-mode supported';
		document.getElementById('m_desc').innerHTML = 'Free 50 GB - End-to-end encryption';
	}
	else document.body.className = 'another-os full-mode unsupported';

	if (app)
	{
		document.getElementById('m_appbtn').href = app;
		document.getElementById('m_title').innerHTML = 'Install the free MEGA app';
	}
	else
	{
		document.getElementById('m_title').innerHTML = 'A dedicated app for your device will be available soon.';
		document.getElementById('m_desc').innerHTML = 'Follow us on Twitter or Facebook for updates.';
	}
	if (window.location.hash.substr(1,1) == '!')
	{
		if (app) document.getElementById('m_title').innerHTML = 'Install the free MEGA app to access this file from your mobile';
		if (ua.indexOf('chrome') > -1)
		{
			setTimeout(function()
			{
				if (confirm('Do you already have the MEGA app installed?')) document.location = 'mega://' + window.location.hash;
			},2500);
		}
		else document.getElementById('m_iframe').src = 'mega://' + window.location.hash;
	}
	else if (window.location.hash.substr(1,7) == 'confirm')
	{
		var i=0;
		if (ua.indexOf('iphone') > -1 || ua.indexOf('ipad') > -1 || ua.indexOf('ipod') > -1) i=1;
		if (ua.indexOf('chrome') > -1) window.location ='mega://' + window.location.hash.substr(i);
		else document.getElementById('m_iframe').src = 'mega://' + window.location.hash.substr(i);
	}
	if (mobileblog)
	{
		document.body.innerHTML = '';
		var script = document.createElement('script');
		script.type = "text/javascript";
		document.head.appendChild(script);
		script.src = 'https://mega.co.nz/blog.js';
	}
}
else if (page == '#android')
{
	document.location = 'https://play.google.com/store/apps/details?id=com.flyingottersoftware.mega';
}
else
{
	if (!b_u)
	{
		/*
			window.onerror = function __MEGAExceptionHandler(msg, url, ln, cn, errobj)
			{
				if (d)
				{
					console.error('Uncaught Exception', msg, url+':'+ln+','+cn, errobj);
				}
				else
				{
					// TODO: XHR to log server?
				}

				return false;
			};
		*/
		
		if (typeof console == "undefined") { this.console = { log: function() {}, error: function() {}}; }
		var d = localStorage.d || 0;
		var jj = localStorage.jj || 0;
		var languages = {'en':['en','en-'],'es':['es','es-'],'fr':['fr','fr-'],'de':['de','de-'],'it':['it','it-'],'nl':['nl','nl-'],'pt':['pt'],'br':['pt-br'],'dk':['da'],'se':['sv'],'fi':['fi'],'no':['no'],'pl':['pl'],'cz':['cz','cz-'],'sk':['sk','sk-'],'sl':['sl','sl-'],'hu':['hu','hu-'],'jp':['ja'],'cn':['zh','zh-cn'],'ct':['zh-hk','zh-sg','zh-tw'],'kr':['ko'],'ru':['ru','ru-mo'],'ar':['ar','ar-'],'he':['he'],'id':['id'],'ca':['ca','ca-'],'eu':['eu','eu-'],'af':['af','af-'],'bs':['bs','bs-'],'sg':[],'tr':['tr','tr-'],'mk':[],'hi':[],'hr':['hr'],'ro':['ro','ro-'],'uk':['||'],'gl':['||'],'sr':['||'],'lt':['||'],'th':['||'],'lv':['||'],'fa':['||'],'ee':['et'],'ms':['ms'],'cy':['cy'],'bg':['bg'],'be':['br'],'tl':['en-ph'],'ka':['||']};

		function detectlang()
		{
			return 'en';
			if (!navigator.language) return 'en';
			var bl = navigator.language.toLowerCase();
			var l2 = languages;
			for (var l in l2) for (b in l2[l]) if (l2[l][b] == bl) return l;
			for (var l in l2) for (b in l2[l]) if (l2[l][b].substring(0,3)==bl.substring(0,3)) return l;
			return 'en';
		}
		var init_f = [];
		var lang = detectlang();
		if ((typeof localStorage != 'undefined') && (localStorage.lang)) if (languages[localStorage.lang]) lang = localStorage.lang;
		var langv = '';
		if (typeof lv != 'undefined') langv = '_' + lv[lang];
		var jsl = [];

		jsl.push({f:'lang/' + lang + langv + '.json', n: 'lang', j:3});
		jsl.push({f:'sjcl_1.js', n: 'sjcl_js', j:1}); // Will be replaced with asmCrypto soon
		jsl.push({f:'js/asmcrypto_6.js',n:'asmcrypto_js',j:1,w:1});
		jsl.push({f:'js/tlvstore_3.js', n: 'tlvstore_js', j:1});
        jsl.push({f:'js/crypto_44.js', n: 'crypto_js', j:1,w:5});
		
		jsl.push({f:'js/jsbn_1.js', n: 'jsbn_js', j:1,w:2});
		jsl.push({f:'js/jsbn2_1.js', n: 'jsbn2_js', j:1,w:2});
		jsl.push({f:'js/jodid25519_3.js', n: 'jodid25519_js', j:1,w:7});
		
		jsl.push({f:'js/user_11.js', n: 'user_js', j:1});
		jsl.push({f:'js/hex_2.js', n: 'hex_js', j:1});
		jsl.push({f:'js/functions_25.js', n: 'functions_js', j:1});
		jsl.push({f:'js/mouse_6.js', n: 'mouse_js', j:1});
		jsl.push({f:'js/jquery-min-1_1.8_1.1_1.js', n: 'jquery', j:1,w:9});
		jsl.push({f:'js/jquery-ui_1.js', n: 'jqueryui_js', j:1,w:12});
		jsl.push({f:'js/base64_1.js', n: 'base64_js', j:1});
		jsl.push({f:'js/filedrag_10.js', n: 'filedrag_js', j:1});
		jsl.push({f:'js/jquery_1.remove_1.js', n: 'jqueryremove_js', j:1});
		jsl.push({f:'js/jquery_2.mousewheel_2.js', n: 'jquerymouse_js', j:1});
		jsl.push({f:'js/jquery_2.jscrollpane_2.min_2.js', n: 'jscrollpane_js', j:1});
		jsl.push({f:'js/mDB_13.js', n: 'mDB_js', j:1});
		jsl.push({f:'js/cleartemp_3.js', n: 'cleartemp_js', j:1});
		jsl.push({f:'js/thumbnail_12.js', n: 'thumbnail_js', j:1});
		jsl.push({f:'js/exif_1.js', n: 'exif_js', j:1,w:3});
		jsl.push({f:'js/megapix_1.js', n: 'megapix_js', j:1});
		jsl.push({f:'js/mega_59.js', n: 'mega_js', j:1,w:7});
		jsl.push({f:'js/chat_1.js', n: 'chat_js', j:1,w:7});
		jsl.push({f:'js/fm_59.js', n: 'fm_js', j:1,w:12});
		jsl.push({f:'js/filetypes_3.js', n: 'filetypes_js', j:1});
		/* better download */
		jsl.push({f:'js/xhr_4.js', n: 'xhr_js', j:1});
		jsl.push({f:'js/events_4.js', n: 'events', j:1,w:4});
		jsl.push({f:'js/queue_14.js', n: 'queue', j:1,w:4});
		jsl.push({f:'js/downloadChrome_10.js', n: 'dl_chrome', j:1,w:3});
		if (is_chrome_firefox && parseInt(Services.appinfo.version) > 27 && localStorage.fxio)
		{
			is_chrome_firefox |= 4;
			jsl.push({f:'js/downloadFirefox_3.js', n: 'dl_firefox', j:1,w:3});
		}
		else
		{
			jsl.push({f:'js/downloadMemory_5.js', n: 'dl_memory', j:1,w:3});
			jsl.push({f:'js/downloadFlash_3.js', n: 'dl_flash', j:1,w:3});
		}
		jsl.push({f:'js/downloader_24.js', n: 'dl_downloader', j:1,w:3});
		jsl.push({f:'js/download2_16.js', n: 'dl_js', j:1,w:3});
		jsl.push({f:'js/upload2_12.js', n: 'upload_js', j:1,w:2});
		/* end better download */
		jsl.push({f:'index_35.js', n: 'index', j:1,w:4});
		jsl.push({f:'html/start_3.html', n: 'start', j:0});
		jsl.push({f:'html/megainfo_1.html', n: 'megainfo', j:0});
		jsl.push({f:'html/js/start_3.js', n: 'start_js', j:1});
		jsl.push({f:'html/bottom2_3.html', n: 'bottom2',j:0});
		jsl.push({f:'html/key_5.html', n: 'key', j:0});
		jsl.push({f:'html/js/key_4.js', n: 'key_js', j:1});
		jsl.push({f:'html/pro_3.html', n: 'pro', j:0});
		jsl.push({f:'html/js/pro_7.js', n: 'pro_js', j:1});
		jsl.push({f:'html/login_2.html', n: 'login', j:0});
		jsl.push({f:'html/js/login_3.js', n: 'login_js', j:1});
		jsl.push({f:'html/fm_17.html', n: 'fm', j:0,w:3});
		jsl.push({f:'html/top_8.html', n: 'top', j:0});
		jsl.push({f:'js/notifications_11.js', n: 'notifications_js', j:1});
		jsl.push({f:'css/style_47.css', n: 'style_css', j:2,w:30,c:1,d:1,cache:1});
		jsl.push({f:'js/avatar_2.js', n: 'avatar_js', j:1,w:3});
		jsl.push({f:'js/countries_3.js', n: 'countries_js', j:1});
		jsl.push({f:'html/dialogs_13.html', n: 'dialogs', j:0,w:2});
		jsl.push({f:'html/transferwidget_1.html', n: 'transferwidget', j:0});
		jsl.push({f:'js/checkboxes_1.js', n: 'checkboxes_js', j:1});
		jsl.push({f:'js/Int64_1.js', n: 'int64_js', j:1});
		jsl.push({f:'js/zip64_10.js', n: 'zip_js', j:1});
		var jsl2 =
		{
			'about': {f:'html/about_6.html', n: 'about', j:0},
			'blog': {f:'html/blog_2.html', n: 'blog', j:0},
			'blog_js': {f:'html/js/blog_22.js', n: 'blog_js', j:1},
			'blogarticle': {f:'html/blogarticle_1.html', n: 'blogarticle', j:0},
			'blogarticle_js': {f:'html/js/blogarticle_1.js', n: 'blogarticle_js', j:1},
			'register': {f:'html/register_1.html', n: 'register', j:0},
			'register_js': {f:'html/js/register_8.js', n: 'register_js', j:1},
			'resellers': {f:'html/resellers_5.html', n: 'resellers', j:0},
			'download': {f:'html/download_17.html', n: 'download', j:0},
			'download_js': {f:'html/js/download_22.js', n: 'download_js', j:1},
			'copyright': {f:'html/copyright_1.html', n: 'copyright', j:0},
			'copyrightnotice': {f:'html/copyrightnotice_2.html', n: 'copyrightnotice', j:0},
			'copyrightnotice_js': {f:'html/js/copyrightnotice_2.js', n: 'copyrightnotice_js', j:1},
			'privacy': {f:'html/privacy_2.html', n: 'privacy', j:0},
			'terms': {f:'html/terms_3.html', n: 'terms', j:0},
			'backup': {f:'html/backup_2.html', n: 'backup', j:0},
			'backup_js': {f:'html/js/backup_3.js', n: 'backup_js', j:1},
			'reset': {f:'html/reset_2.html', n: 'reset', j:0},
			'reset_js': {f:'html/js/reset_3.js', n: 'reset_js', j:1},
			'filesaver': {f:'js/filesaver_1.js', n: 'filesaver', j:1},
			'recovery': {f:'html/recovery_2.html', n: 'recovery', j:0},
			'recovery_js': {f:'html/js/recovery_3.js', n: 'recovery_js', j:1},
			'credits': {f:'html/credits_2.html', n: 'credits', j:0},
			'takedown': {f:'html/takedown_15.html', n: 'takedown', j:0},
			'dev': {f:'html/dev_2.html', n: 'dev', j:0},
			'arkanoid_js': {f:'js/arkanoid_1.js', n: 'arkanoid_js', j:1},
			'dev_js': {f:'html/js/dev_2.js', n: 'dev_js', j:1},
			'sdkterms': {f:'html/sdkterms_2.html', n: 'sdkterms', j:0},
			'help': {f:'html/help_2.html', n: 'help', j:0},
			'help_js': {f:'html/js/help_12.js', n: 'help_js', j:1},
			'firefox': {f:'html/firefox_1.html', n: 'firefox', j:0},
			'sync': {f:'html/sync_7.html', n: 'sync', j:0},
			'sync_js': {f:'html/js/sync_4.js', n: 'sync_js', j:1},
			'mobile': {f:'html/mobile_5.html', n: 'mobile', j:0},
			'affiliates': {f:'html/affiliates_1.html', n: 'affiliates', j:0},
			'affiliate_js': {f:'html/js/affiliate_2.js', n: 'affiliate_js', j:0},
			'affiliateterms': {f:'html/affiliateterms_2.html', n: 'affiliateterms', j:0},
			'affiliatesignup': {f:'html/affiliatesignup_1.html', n: 'affiliatesignup', j:0},
			'affiliatesignup_js': {f:'html/js/affiliatesignup_3.js', n: 'affiliatesignup_js', j:1},
			'affiliatemember': {f:'html/affiliatemember_2.html', n: 'affiliatemember', j:0},
			'affiliatemember_js': {f:'html/js/affiliatemember_3.js', n: 'affiliatemember_js', j:1},
			'contact': {f:'html/contact_1.html', n: 'contact', j:0},
			'privacycompany': {f:'html/privacycompany_1.html', n: 'privacycompany', j:0},
			'chrome': {f:'html/chrome_4.html', n: 'chrome', j:0},
			'zxcvbn_js': {f:'js/zxcvbn_1.js', n: 'zxcvbn_js', j:1},
			'mads_js': {f:'js/mads_2.js', n: 'mads_js', j:1}
		};
		var subpages =
		{
			'about': ['about'],
			'terms': ['terms'],
			'credits': ['credits'],
			'backup': ['backup','backup_js','filesaver'],			
			'recovery': ['recovery','recovery_js'],
			'reset': ['reset','reset_js'],
			'blog': ['blog','blog_js','blogarticle','blogarticle_js'],
			'register': ['register','register_js'],
			'android': ['android'],
			'resellers': ['resellers'],
			'!': ['download','download_js'],
			'copyright': ['copyright'],
			'key':['arkanoid_js'],
			'copyrightnotice': ['copyrightnotice','copyrightnotice_js'],
			'privacy': ['privacy','privacycompany'],
			'takedown': ['takedown'],
			'firefox': ['firefox'],
			'mobile': ['mobile'],
			'sync': ['sync','sync_js'],
			'contact': ['contact'],
			'dev': ['dev','dev_js','sdkterms'],
			'sdk': ['dev','dev_js','sdkterms'],
			'doc': ['dev','dev_js','sdkterms'],
			'help': ['help','help_js'],
			'chrome': ['chrome'],
			'plugin': ['chrome','firefox'],
			'affiliate': ['affiliates','affiliateterms','affiliatesignup','affiliatesignup_js','affiliatemember','affiliatemember_js','affiliate_js'],
			'recover': ['reset','reset_js']
		};

	    if (page && page.indexOf('%21') > -1) document.location.hash = page.replace('%21','!').replace('%21','!');
		
		if (page) page = page.replace('#','').replace('%21','!');


		for (var p in subpages)
		{
			if (page && page.substr(0,p.length) == p)
			{
				for (i in subpages[p]) jsl.push(jsl2[subpages[p][i]]);
			}
		}
		var downloading = false;
		var ul_uploading = false;
		var lightweight=false;
		var njsl = [];
		var fx_startup_cache = is_chrome_firefox && nocontentcheck;
		if ((typeof Worker != 'undefined') && (typeof window.URL != 'undefined') && !fx_startup_cache)
		{
			var hashdata = ['self.postMessage = self.webkitPostMessage || self.postMessage;',sjcl_sha_js,'self.onmessage = function(e) { try { e.data.hash = sha256(e.data.text);  self.postMessage(e.data); } catch(err) { e.data.error = err.message; self.postMessage(e.data);  } };'];
			try  { var blob = new Blob(hashdata, { type: "text/javascript" }); }
			catch(e)
			{
				window.BlobBuilder = window.BlobBuilder || window.WebKitBlobBuilder || window.MozBlobBuilder || window.MSBlobBuilder;
				var bb = new BlobBuilder();
				for (var i in hashdata) bb.append(hashdata[i]);
				var blob = bb.getBlob('text/javascript');
			}
			var hash_url = window.URL.createObjectURL(blob);
			var hash_workers = [];
			var i =0;
			while (i < 2)
			{
				try
				{
					hash_workers[i] = new Worker(hash_url);
					hash_workers[i].postMessage = hash_workers[i].webkitPostMessage || hash_workers[i].postMessage;
					hash_workers[i].onmessage = function(e)
					{
						if (e.data.error)
						{
							console.log('error',e.data.error);
							console.log(e.data.text);
							alert('error');
						}
						if (!nocontentcheck && !cmparrays(e.data.hash,sh1[jsl[e.data.jsi].f]))
						{
							if (bootstaticpath.indexOf('cdn') > -1)
							{
								sessionStorage.skipcdn=1;
								document.location.reload();
							}
							else alert('An error occurred while loading MEGA. The file ' + bootstaticpath+jsl[e.data.jsi].f + ' is corrupt. Please try again later. We apologize for the inconvenience.');

							contenterror=1;
						}
						if (!contenterror)
						{
							jsl_current += jsl[e.data.jsi].w || 1;
							jsl_progress();
							if (++jslcomplete == jsl.length) initall();
							else jsl_load(e.data.xhri);
						}
					};
				}
				catch(e)
				{
					hash_workers = undefined;
				}
				i++;
			}
		}
		var pages = [], scripts = {};
		function getxhr()
		{
			return (typeof XDomainRequest != 'undefined' && typeof ArrayBuffer == 'undefined') ? new XDomainRequest() : new XMLHttpRequest();
		}

		var xhr_progress,xhr_stack,jsl_fm_current,jsl_current,jsl_total,jsl_perc,jsli,jslcomplete;

		function jsl_start()
		{
			jslcomplete = 0;
			xhr_progress = [0,0];
			xhr_stack = Array(xhr_progress.length);
			jsl_fm_current = 0;
			jsl_current = 0;
			jsl_total = 0;
			jsl_perc = 0;
			jsli=0;
			for (var i = jsl.length; i--;) if (!jsl[i].text) jsl_total += jsl[i].w || 1;
			if (fx_startup_cache)
			{
				var step = function(jsi)
				{
					jsl_current += jsl[jsi].w || 1;
					jsl_progress();
					if (++jslcomplete == jsl.length) initall();
					else
					{
						// mozRunAsync(next.bind(this, jsli++));
						next(jsli++);
					}
				};
				var next = function(jsi)
				{
					var file = bootstaticpath + jsl[jsi].f;									

					if (jsl[jsi].j == 1)
					{
						try
						{
							loadSubScript(file);
						}
						catch(e)
						{
							Cu.reportError(e);

							alert('An error occurred while loading MEGA.\n\nFilename: '
								+ file + "\n" + e + '\n\n' + mozBrowserID);
						}
						step(jsi);
					}
					else
					{
						var ch = NetUtil.newChannel(file);
						ch.contentType = jsl[jsi].j == 3
							? "application/json":"text/plain";

						NetUtil.asyncFetch(ch, function(is, s)
						{
							if (!Components.isSuccessCode(s))
							{
								alert('An error occurred while loading MEGA.' +
									' The file ' + file + ' could not be loaded.');
							}
							else
							{
								jsl[jsi].text = NetUtil.readInputStreamToString(is, is.available());
								if (jsl[jsi].j == 3) l = JSON.parse(jsl[jsi].text);
								step(jsi);
							}
						});
					}
				};
				next(jsli++);
			}
			else
			{
				for (var i = xhr_progress.length; i--; ) jsl_load(i);
			}
		}

		var xhr_timeout=30000;

		function xhr_error()
		{
			xhr_timeout+=10000;
			console.log(xhr_timeout);
			if (bootstaticpath.indexOf('cdn') > -1)
			{
				bootstaticpath = geoStaticpath(1);
				staticpath = geoStaticpath(1);
			}
			xhr_progress[this.xhri] = 0;
			xhr_load(this.url,this.jsi,this.xhri);
		}

		function xhr_load(url,jsi,xhri)
		{
			  xhr_stack[xhri] = getxhr();
			  xhr_stack[xhri].onload = function()
			  {
				jsl[this.jsi].text = this.response || this.responseText;

				if (typeof hash_workers != 'undefined' && !nocontentcheck)
				{
					hash_workers[this.xhri].postMessage({'text':jsl[this.jsi].text,'xhr':'test','jsi':this.jsi,'xhri':this.xhri});
				}
				else
				{
					if (!nocontentcheck && !cmparrays(sha256(jsl[this.jsi].text),sh1[jsl[this.jsi].f]))
					{
						alert('An error occurred while loading MEGA. The file ' + bootstaticpath+jsl[this.jsi].f + ' is corrupt. Please try again later. We apologize for the inconvenience.');
						contenterror=1;
					}
					if (!contenterror)
					{
						jsl_current += jsl[this.jsi].w || 1;
						jsl_progress();
						if (++jslcomplete == jsl.length) initall();
						else jsl_load(this.xhri);
					}
				}
			  }
			  xhr_stack[xhri].onreadystatechange = function()
			  {
				try
				{
					if (this.readyState == 1) this.timeout=0;
				}
				catch(e)
				{
				
				}				
			  }
			  xhr_stack[xhri].onerror = xhr_error;
			  xhr_stack[xhri].ontimeout = xhr_error;
			  if (jsl[jsi].text)
			  {
				if (++jslcomplete == jsl.length) initall();
				else jsl_load(xhri);
			  }
			  else
			  {
				  xhr_stack[xhri].url = url;
				  xhr_stack[xhri].jsi = jsi;
				  xhr_stack[xhri].xhri = xhri;
				  if (localStorage.dd) url += '?t=' + Date.now();
				  xhr_stack[xhri].open("GET", (!localStorage.dd && url.indexOf('mads_') > -1 ? 'https://eu.static.mega.co.nz/' : bootstaticpath) + url, true);
				  xhr_stack[xhri].timeout = xhr_timeout;
				  if (is_chrome_firefox) xhr_stack[xhri].overrideMimeType('text/plain');
				  xhr_stack[xhri].send(null);				  
			  }
		}
		window.onload = function ()
		{
			if (!maintenance && !androidsplash) jsl_start();
		}
		function jsl_load(xhri)
		{
			if (jsl[jsli]) 
			{
				xhr_load(jsl[jsli].f, jsli++,xhri);
			}
		}
		function jsl_progress()
		{
			if (d) console.log('done',(jsl_current+jsl_fm_current));
			if (d) console.log('total',jsl_total);
			var p = Math.floor((jsl_current+jsl_fm_current)/jsl_total*100);
			if ((p > jsl_perc) && (p <= 100))
			{
				jsl_perc = p;
				if (is_extension) p=100;
				document.getElementById('loadinganim').className = 'loading-progress-bar percents-'+p;
			}
		}
		var jsl_loaded={};
		function initall()
		{
			var jsar = [];
			var cssar = [];
			for(var i in localStorage) if (i.substr(0,6) == 'cache!') delete localStorage[i];
			for (var i in jsl)
			{
			  jsl_loaded[jsl[i].n]=1;
			  if ((jsl[i].j == 1) && (!jj))
			  {
				if (!fx_startup_cache)
				{
					if (window.URL) jsar.push(jsl[i].text + '\n\n');
					else evalscript(jsl[i].text);
				}
			  }
			  else if ((jsl[i].j == 2) && (!jj))
			  {
				if (document.getElementById('bootbottom')) document.getElementById('bootbottom').style.display='none';
				if (window.URL)
				{
					cssar.push(jsl[i].text.replace(/\.\.\//g,staticpath).replace(new RegExp( "\\/en\\/", "g"),'/' + lang + '/'));
				}
				else
				{
					var css = document.createElement('style');
					css.type = "text/css";
					css.rel = 'stylesheet';
					document.getElementsByTagName('head')[0].appendChild(css);
					css.innerHTML = jsl[i].text.replace(/\.\.\//g,staticpath).replace(new RegExp( "\\/en\\/", "g"),'/' + lang + '/');
				}
			  }
			  else if (jsl[i].j == 3) l = JSON.parse(jsl[i].text);
			  else if (jsl[i].j == 4) scripts[jsl[i].f] = jsl[i].text;
			  else if (jsl[i].j == 0) pages[jsl[i].n] = jsl[i].text;
			}
			if (window.URL)
			{
				try
				{
					var blob = new Blob(cssar, { type: "text/css" });
					for ( var f in scripts ) {
						if (!scripts[f].match(/^blob:/)) {
							scripts[f] = window.URL.createObjectURL( new Blob( [ scripts[f] ], { type: 'text/javascript' } ) );
						}
					}
				}
				catch(e)
				{
					window.BlobBuilder = window.BlobBuilder || window.WebKitBlobBuilder || window.MozBlobBuilder || window.MSBlobBuilder;
					var bb = new BlobBuilder();
					for (var i in cssar) bb.append(cssar[i]);
					var blob = bb.getBlob('text/css');
					for ( var f in scripts ) {
						if (!scripts[f].match(/^blob:/)) {
							bb = new BlobBuilder();
							bb.append( scripts[f] );
							scripts[f] = window.URL.createObjectURL( bb.getBlob('text/javascript') );
						}
				    }
				}
				var link = document.createElement('link');
				link.setAttribute('rel', 'stylesheet');
				link.type = 'text/css';
				link.href = window.URL.createObjectURL(blob);
				document.head.appendChild(link);
				cssar=undefined;
				jsar.push('jsl_done=true; boot_done();');
				evalscript_url(jsar);
				jsar=undefined;
			}
			else
			{
				jsl_done=true;
				boot_done();
			}
		}
	}
	if (ua.indexOf('android') > 0 && !sessionStorage.androidsplash && document.location.hash.indexOf('#confirm') == -1)
	{
		if (document.location.hash == '#android')
		{
			document.location = 'https://play.google.com/store/apps/details?id=com.flyingottersoftware.mega';
		}
		else
		{
			document.write('<link rel="stylesheet" type="text/css" href="' + staticpath + 'resources/css/mobile-android.css" /><div class="overlay"></div><div class="new-folder-popup" id="message"><div class="new-folder-popup-bg"><div class="new-folder-header">MEGA for Android</div><div class="new-folder-main-bg"><div class="new-folder-descr">Do you want to install the latest<br/> version of the MEGA app for Android?</div><a class="new-folder-input left-button" id="trashbinYes"> <span class="new-folder-bg1"> <span class="new-folder-bg2" id="android_yes"> Yes </span> </span></a><a class="new-folder-input right-button" id="trashbinNo"> <span class="new-folder-bg1"> <span class="new-folder-bg2" id="android_no">No </span> </span></a><div class="clear"></div></div></div></div></div>');
			document.getElementById('android_yes').addEventListener("click", function ()
			{
				document.location = 'https://play.google.com/store/apps/details?id=com.flyingottersoftware.mega';
			}, false);
			document.getElementById('android_no').addEventListener("click", function ()
			{
				sessionStorage.androidsplash=1;
				document.location.reload();
			}, false);
			androidsplash=true;
		}
	}
	else
	{
		var istaticpath = staticpath;
		if (document.location.href.substr(0,19) == 'chrome-extension://')  istaticpath = '../';
		else if (is_chrome_firefox) istaticpath = 'chrome://mega/content/';

		document.write('<style type="text/css">.div, span, input {outline: none;}.hidden {display: none;}.clear {clear: both;margin: 0px;padding: 0px;display: block;}.loading-main-block {width: 100%;height: 100%;overflow: auto;font-family:Arial, Helvetica, sans-serif;}.loading-mid-white-block {height: 100%;width:100%;}.mid-centered-block {position: absolute;width: 494px;min-height: 158px;top: 50%;left: 50%;margin: -95px 0 0 -247px;}.loading-main-bottom {max-width: 940px;width: 100%;position: absolute;bottom: 20px;left: 50%;margin: 0 0 0 -470px;text-align: center;}.loading-bottom-button {height: 29px;width: 29px;float: left;background-image: url(' + istaticpath + 'images/mega/loading-sprite.png);background-repeat: no-repeat;cursor: pointer;}.loading-bottom-button.st-facebook-button {float: right;background-position: -40px -2376px;margin-left: 11px;}.loading-bottom-button.st-facebook-button:hover {background-position: -40px -2336px;}.loading-bottom-button.st-twitter-button {float: right;background-position: -1px -2376px;margin-left: 11px;}.loading-bottom-button.st-twitter-button:hover {background-position: -1px -2336px;}.loading-cloud {width: 222px;height: 158px;background-image: url(' + istaticpath + 'images/mega/loading-sprite.png);background-repeat: no-repeat;background-position: 0 -2128px;margin: 0 auto;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;-ms-box-sizing: border-box;box-sizing: border-box;padding-top: 55px;}.loading-progress-bar, .loading-progress-bar div {width: 80px;height: 80px;margin: 0 0 0 71px;background-image: url(' + istaticpath + 'images/mega/loading-sprite.png);background-repeat: no-repeat;background-position: 0 top;}.loading-progress-bar div {background-position: -71px -2183px;margin: 0;}.maintance-block {position: absolute;width: 484px;min-height: 94px;border: 2px solid #d9d9d9;-moz-border-radius: 7px;-webkit-border-radius: 7px;border-radius: 7px;padding: 10px;color: #333333;font-size: 13px;line-height: 30px;padding: 15px 15px 15px 102px;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;-ms-box-sizing: border-box;box-sizing: border-box;background-image: url(' + istaticpath + 'images/mega/loading-sprite.png);background-repeat: no-repeat;background-position: -60px -2428px;margin-top: 45px;}.loading-progress-bar.percents-0 {background-position: 0 0;}.loading-progress-bar.percents-1, .loading-progress-bar.percents-2, .loading-progress-bar.percents-3 {background-position: -130px 0;}.loading-progress-bar.percents-4, .loading-progress-bar.percents-5, .loading-progress-bar.percents-6 {background-position: 0 -100px;}.loading-progress-bar.percents-7, .loading-progress-bar.percents-8, .loading-progress-bar.percents-9 {background-position: -130px -100px;}.loading-progress-bar.percents-10, .loading-progress-bar.percents-11, .loading-progress-bar.percents-12 {background-position: 0 -200px;}.loading-progress-bar.percents-13, .loading-progress-bar.percents-14, .loading-progress-bar.percents-15 {background-position: -130px -200px;}.loading-progress-bar.percents-16, .loading-progress-bar.percents-17, .loading-progress-bar.percents-18 {background-position: 0 -300px;}.loading-progress-bar.percents-19, .loading-progress-bar.percents-20, .loading-progress-bar.percents-21 {background-position: -130px -300px;}.loading-progress-bar.percents-22, .loading-progress-bar.percents-23, .loading-progress-bar.percents-24 {background-position: 0 -400px;}.loading-progress-bar.percents-25, .loading-progress-bar.percents-26, .loading-progress-bar.percents-27 {background-position: -130px -400px;}.loading-progress-bar.percents-28, .loading-progress-bar.percents-29, .loading-progress-bar.percents-30 {background-position: 0 -500px;}.loading-progress-bar.percents-31, .loading-progress-bar.percents-32, .loading-progress-bar.percents-33 {background-position: -130px -500px;}.loading-progress-bar.percents-34, .loading-progress-bar.percents-35 {background-position: 0 -600px;}.loading-progress-bar.percents-36, .loading-progress-bar.percents-37 {background-position: -130px -600px;}.loading-progress-bar.percents-38, .loading-progress-bar.percents-39 {background-position: 0 -700px;}.loading-progress-bar.percents-40, .loading-progress-bar.percents-41 {background-position: -130px -700px;}.loading-progress-bar.percents-42, .loading-progress-bar.percents-43 {background-position: 0 -800px;}.loading-progress-bar.percents-44, .loading-progress-bar.percents-45 {background-position: -130px -800px;}.loading-progress-bar.percents-46, .loading-progress-bar.percents-47 {background-position: 0 -900px;}.loading-progress-bar.percents-48, .loading-progress-bar.percents-49 {background-position: -130px -900px;}.loading-progress-bar.percents-50 {background-position: 0 -1000px;}.loading-progress-bar.percents-51, .loading-progress-bar.percents-52, .loading-progress-bar.percents-53 {background-position: -130px -1000px;}.loading-progress-bar.percents-54, .loading-progress-bar.percents-55, .loading-progress-bar.percents-56 {background-position: 0 -1100px;}.loading-progress-bar.percents-57, .loading-progress-bar.percents-58, .loading-progress-bar.percents-59 {background-position: -130px -1100px;}.loading-progress-bar.percents-60, .loading-progress-bar.percents-61, .loading-progress-bar.percents-62 {background-position: 0 -1200px;}.loading-progress-bar.percents-63, .loading-progress-bar.percents-64, .loading-progress-bar.percents-65 {background-position: -130px -1200px;}.loading-progress-bar.percents-66, .loading-progress-bar.percents-67, .loading-progress-bar.percents-68 {background-position: 0 -1300px;}.loading-progress-bar.percents-69, .loading-progress-bar.percents-70, .loading-progress-bar.percents-71 {background-position: -130px -1300px;}.loading-progress-bar.percents-72, .loading-progress-bar.percents-73, .loading-progress-bar.percents-74 {background-position: 0 -1400px;}.loading-progress-bar.percents-75, .loading-progress-bar.percents-76, .loading-progress-bar.percents-77 {background-position: -130px -1400px;}.loading-progress-bar.percents-78, .loading-progress-bar.percents-79, .loading-progress-bar.percents-80 {background-position: 0 -1500px;}.loading-progress-bar.percents-81, .loading-progress-bar.percents-82, .loading-progress-bar.percents-83 {background-position: -130px -1500px;}.loading-progress-bar.percents-84, .loading-progress-bar.percents-85, .loading-progress-bar.percents-86 {background-position: 0 -1600px;}.loading-progress-bar.percents-87, .loading-progress-bar.percents-88, .loading-progress-bar.percents-89 {background-position: -130px -1600px;}.loading-progress-bar.percents-90, .loading-progress-bar.percents-91, .loading-progress-bar.percents-92 {background-position: 0 -1800px;}.loading-progress-bar.percents-93, .loading-progress-bar.percents-94, .loading-progress-bar.percents-95 {background-position: -130px -1800px;}.loading-progress-bar.percents-96, .loading-progress-bar.percents-97 {background-position: 0 -1900px;}.loading-progress-bar.percents-98, .loading-progress-bar.percents-99 {background-position: -130px -1900px;}.loading-progress-bar.percents-100 {background-position: 0 -2000px;}.follow-txt {text-decoration:none; line-height: 28px; float:right; color:#666666; font-size:12px;}@media only screen and (-webkit-min-device-pixel-ratio: 1.5), only screen and (-o-min-device-pixel-ratio: 3/2), only screen and (min--moz-device-pixel-ratio: 1.5), only screen and (min-device-pixel-ratio: 1.5) {.maintance-block, .loading-progress-bar, .loading-progress-bar div, .loading-cloud, .loading-bottom-button {background-image: url(' + istaticpath + 'images/mega/loading-sprite@2x.png);	background-size: 222px auto;	}}</style><div class="loading-main-block" id="loading"><div class="loading-mid-white-block"><div class="mid-centered-block"><div class="loading-cloud"><div class="loading-progress-bar percents-1" id="loadinganim"><div></div></div></div><div class="maintance-block hidden">Scheduled System Maintenance - Expect Disruptions<br/>Sunday 04:00 - 10:00 UTC </div></div><div class="loading-main-bottom" id="bootbottom"><a href="https://www.facebook.com/MEGAprivacy" target="_blank" class="loading-bottom-button st-facebook-button"></a><a href="https://twitter.com/MEGAprivacy" class="loading-bottom-button st-twitter-button"></a><a href="https://www.twitter.com/MEGAprivacy" target="_blank" class="follow-txt" target="_blank">follow us</a><div class="clear"></div></div></div></div>');
	}
	var u_storage,loginresponse,u_sid,jsl_done,dlresponse,dl_res;

	u_storage = init_storage( localStorage.sid ? localStorage : sessionStorage );

	if (u_sid = u_storage.sid)
	{
		loginresponse = true;
		var lxhr = getxhr();
		lxhr.onload = function()
		{
			if (this.status == 200)
			{
				try
				{
					loginresponse = this.response || this.responseText;
					if (loginresponse && loginresponse[0] == '[') loginresponse = JSON.parse(loginresponse);
					else loginresponse = false;
					boot_done();
				}
				catch (e)
				{
					loginresponse= false;
					boot_done();
				}
			}
			else
			{
				loginresponse= false;
				boot_done();
			}
		}
		lxhr.onerror = function()
		{
			loginresponse= false;
			boot_done();
		}
		lxhr.open("POST", apipath + 'cs?id=0&sid='+u_storage.sid, true);
		lxhr.send(JSON.stringify([{'a':'ug'}]));
	}
	function boot_auth(u_ctx,r)
	{
		u_type = r;
		u_checked=true;
		startMega();
	}
	function boot_done()
	{
		lxhr = dlxhr = undefined;
		if (loginresponse === true || dl_res === true || !jsl_done) return;
	    else if (loginresponse)
		{
			api_setsid(u_sid);
			u_checklogin3a(loginresponse[0],{checkloginresult:boot_auth});
		}
		else u_checklogin({checkloginresult:boot_auth},false);
	}
	if (page.substr(0,1) == '!' && page.length > 1)
	{
		var dlxhr = getxhr(),dl_res = true;
		dlxhr.onload = function()
		{
			if (this.status == 200)
			{
				try
				{
					dl_res = this.response || this.responseText;
					if (dl_res[0] == '[') dl_res = JSON.parse(dl_res);
					if (dl_res[0]) dl_res = dl_res[0];
					boot_done();
				}
				catch (e)
				{
					dl_res = false;
					boot_done();
				}
			}
			else
			{
				dl_res = false;
				boot_done();
			}
		}
		dlxhr.onerror = function()
		{
			dl_res= false;
			boot_done();
		}
		dlxhr.open("POST", apipath + 'cs?id=0', true);
		dlxhr.send(JSON.stringify([{'a':'g',p:page.substr(1,8)}]));
	}
}
