require 'spec_helper'

describe "StaticPages" do
  
  describe "Home Page" do
    

    it "should have the content 'Home'" do
    	visit root_path
    	expect(page).to have_content('Home')
    end

    it "should have the title 'Rangaire'" do
    	visit root_path
    	expect(page).to have_title('Rangaire')

  	end
  end

  describe "About Us" do
    

    it "should have the content 'About Us'" do
    	visit about_us_path
    	expect(page).to have_content('About Us')
    end

    it "should have the title 'About Us'" do
    	visit about_us_path
    	expect(page).to have_title('About Us')


  	end
  end

  describe "Contact Us" do
    

    it "should have the content 'Contact Us'" do
    	visit contact_us_path
    	expect(page).to have_content('Contact Us')
    end

    it "should have the title 'Contact us'" do
    	visit contact_us_path
    	expect(page).to have_title('Contact Us')




  	end
  end

 describe "Warranty Page" do
    

    it "should have the content 'Warranty'" do
    	visit warranty_path
    	expect(page).to have_content('Warranty')
    end

    it "should have the title 'Warranty'" do
    	visit warranty_path
    	expect(page).to have_title('Warranty')


  	end
  end


 describe "Products" do
    

    it "should have the content 'Products'" do
    	visit products_path
    	expect(page).to have_content('Products')
    end

    it "should have the title 'Products'" do
    	visit products_path
    	expect(page).to have_title('Products')

    end

  end




end
