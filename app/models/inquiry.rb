# app/models/inquiry.rb

class Inquiry
  extend ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations
  include ActionView::Helpers::TextHelper
  
  attr_accessor :name, :customernumber, :nickname, :email, :customerphonenumber, :besttime, :ordernumber,
                :message, :lastname
  
  validates :name, 
            :presence => true

  validates :lastname, 
            :presence => true
  
  validates :email,
            :format => { :with => /\b[A-Z0-9._%a-z\-]+@(?:[A-Z0-9a-z\-]+\.)+[A-Za-z]{2,4}\z/ }
  
  validates :message,
            :length => { :minimum => 10, :maximum => 1000 }

  validates :nickname,
            :presence => false

  validates :customerphonenumber,
            :presence => true

  validates :besttime,
            :presence => true
  
  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end
  
  def deliver
    return false unless valid?
    Pony.mail({
      :from => %("#{name}" <#{email}>),
      :reply_to => email,
      :subject => "Website inquiry",
      :body => %(
        Name:                  #{name}\s#{lastname}\n
        Customer Number:       #{customernumber}\n
        Email:                 #{email}\n
        Customer Contact:      #{customerphonenumber}\n
        Best Time To Contact:  #{besttime}\n
        Order Number:          #{ordernumber}\n
        Message:               #{message})

      
      
      })
  end
      
  def persisted?
    false
  end
end