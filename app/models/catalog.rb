class Catalog < ActiveRecord::Base
	has_attached_file :catalog
  	validates_attachment_file_name :catalog, :matches => [/pdf\Z/]
end
