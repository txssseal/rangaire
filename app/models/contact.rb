class Contact < MailForm::Base 
	attribute :name, 	:validate => true
	attribute :email,	:validate => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/
	attribute :message
	attribute :nickname, :captcha => true

	#Declare the email-header. it accepts anything the mail method in ActionMailer accepts

	def headers
		{
			:subject => "My Contact Form",
			:to => "colton@coltonseal.com",
			:from => %("#{name}" <#{email}>)
		}
			
	end	
end
	