class Product < ActiveRecord::Base
	
	has_attached_file :spec
  	validates_attachment_file_name :spec, :matches => [/pdf\Z/]

  	self.per_page = 20

  	searchable do
    	text :spec_name, :boost => 10
    end
end