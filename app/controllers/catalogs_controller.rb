class CatalogsController < ApplicationController
	before_action :authenticate_user!, only: [:new, :edit, :destroy, :update]
  	before_action :set_product, only: [:show, :edit, :update, :destroy]


	def index
		@catalog = Catalog.all
	end

	def create
		@catalog = Catalog.new(product_params)

		if @catalog.save
      		redirect_to @catalog, notice: 'Product was successfully created.'
    	else
      		render action: 'new'
    	end
	end

	def update
		if @catalog.update(catalog_params)
	      redirect_to @catalog, notice: 'Product was successfully updated.'
	    else
	      render action: 'edit'
	    end
	end

	def new
		@catalog = Catalog.new
	end

	def destroy
	    @catalog.destroy
	    redirect_to root_url, notice: 'Product was successfully destroyed.'
	end

private
    # Use callbacks to share common setup or constraints between actions.
    def set_catalog
      @catalog = Catalog.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def catalog_params
      params.require(:catalog).permit(:catalog)
    end

end
