class StaticPagesController < ApplicationController
  def home
  end

  def about
  end

  def products
  end

  def contact
  end

  def warranty
  end
end
