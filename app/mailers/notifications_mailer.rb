class NotificationsMailer < ActionMailer::Base

  default :from => "coltonsealtt@gmail.com"
  default :to => "coltonsealtt@gmail.com"

  def new_message(message)
    @message = message
    mail(:subject => "[coltonseal.com] #{message.subject}")
  end

end